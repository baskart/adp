var mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/mydb');

var mongoSchema = mongoose.Schema;

var userSchema = {
    "description": String,
    "event":String,
    "main_speaker":String,
    "name":String,
    "published_date":String,
    "ratings":String,
    "related_talks":String,
    "speaker_occupation":String,
    "tags":String,
    "title":String,
    "url":String,
     views:Number
};

module.exports = mongoose.model('tedtalks',userSchema);