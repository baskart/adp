const express = require('express');
const mongoF = require("./mongo");
const app = express()
const router = express.Router()

router.get('/users',(req,res) => {
    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo <0 || pageNo === 0 ){
        response = {"error": true, "message":"invalid page number, should start with 1"};
        return res.json(response)
    } 
    query.skip = size *(pageNo -1)
    query.limit = size

    mongoF.find({},{}, query, function(err,data){
        if(err){
            response = {"error": true, "message":"Error Fetching data"};
        } else {
            response = data
        }
        res.json(response)
    });
})

app.use('/api',router)
app.listen(3030)